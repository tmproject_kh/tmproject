# 영화 대본을 통한 캐릭터들간의 관계도 분석
---------------
## 프로젝트 목표
--------------------
>저희는 프로젝트 목표로, 팀원들의 공통 관심분야로서 보다 많은 정보와 소통이 오갈 수 있는 영화를 분석하기로 했습니다. 영화를 볼 때 인물들간의 관계가 한 눈에 보였으면 좋겠다는 생각이 들 때가 있습니다. 인터넷을 검색해서 보여주는 인물들 간의 관계를 정리해둔 포스트를 봐도 각 인물들을 분석하면서 설명하다보니 관계를 한눈에 인식하지 못하는 경우도 있어 여러번 다시 보게 되는 경우도 있었습니다. 이에 저희는 인물들 간의 감정 변화나 관계도에 대해 간결하게 보여주는 것이 있었으면 좋겠다고 생각하여 프로젝트를 진행하게 되었습니다.
영화의 서사는 일반적으로 갈등의 발생으로 인해 전개되며, 갈등의 해소와 함께 마무리되는 구조입니다. 이 갈등은 극의 상황, 그리고 '등장인물의 감정' 과 연관성이 큽니다. 등장인물의 감정 상태와 그것의 변화로 다른 등장인물과의 상호 관계가 바뀌면서 갈등이 발생하거나 해소되기 때문입니다.
이것을 토대로 인물의 감정을 분석한다면 다른 등장인물과의 상호 관계성, 극에서 맡은 역할, 나아가 극의 분위기를 파악함으로써 영화의 장르까지 유추하여 다양한 결과물을 얻어내는 것이 목표입니다.

------------
## 프로젝트 정보
------------
### 의존성 (dependency)
------------
    python 3.x

-----------
### 사용한 데이터 셋
---------------------
> + 네이버 영화 리뷰 감성 분석 데이터 <https://github.com/e9t/nsmc/>
> + KNU 한국어 감성사전 <https://github.com/park1200656/KnuSentiLex>
> + 건축학개론 대본 데이터 <https://www.filmmakers.co.kr/koreanScreenplays/3547349>
> + 부산행 대본 데이터 <https://www.filmmakers.co.kr/koreanScreenplays/5691484>

-----------------
### 프로젝트 용도
---------------------------
* 영화에서 등장한 긍부정 단어를 추출
 - 긍정적/부정적 대사를 한 인물의 극 중 긍정적/부정적 위치 파악
 - 문장 추출에 이용
   + 긍정적/부정적 문장이 나온 횟수를 분석하여 영화의 분위기 파악
* 인물의 대사를 추출하여 유사도 분석으로 상호 관계성 파악
---------------------------------

## 프로젝트 과정
-----------------
### 등장인물 긍부정 대사 빈도수 추출
-----------------------------------------
#### 건축학개론

```
#긍정문 문장들만 뽑아냄
from pathlib import Path

file = Path.cwd() / './data/pos_sentence.txt'

with open(file, 'r') as text:
    pos_sentence = text.read()
```

```
from konlpy.tag import Mecab
```

```
mecab = Mecab()
print(mecab.morphs(pos_sentence))
#등장인물들을 알아내기 위해 mecab 태거를 이용하여 형태소로 나누어보았다.
#승민이 승, 민으로 출력되는 오류 발견
```

```
pos_sentence_sort = sorted(list(pos_sentence.split("\n")))
print(pos_sentence_sort)
#데이터를 확인해보면 이름이나 컷?의 시작은 탭으로 구분되어있음.
```

```
#탭을 +=으로 수정하고 +단위로 나눈 후 =이 포함되어있는 리스트 요소를 지운다. 
pos_sentence_re = []
for i in pos_sentence_sort:
    sentence = i.split("\t")
    pos_sentence_re.append(sentence)
```

```
print(pos_sentence_re)
#2차원 리스트
```

```
pos_sentence_re = sum(pos_sentence_re,[]) #1차원으로 변경
```

```
character = []
for i in range(0,len(pos_sentence_re)):
    if len(pos_sentence_re[i]) <= 4:
        character.append(pos_sentence_re[i])
```

```
print(character)
```

```
from collections import Counter
count = Counter(character)

name_count = []
names = []

for n, c in count.most_common(100):
    dics = {'name': n, 'count': c}
    if len(dics['name']) >= 2 and len(names) <= 49:
        name_count.append(dics)
        names.append(dics['name'])
```

```
for name in name_count:
    print(" {:<14}".format(name['name']), end='\t')
    print("{}".format(name['count']))
```

> * 위 코드대로 실행할 시 결과, 건축학개론에서 긍정적인 대사를 하는 등장인물은

>지문            	80
 서연            	72
 승민            	37
 은채            	9
 어머니           	7
 납뜩이           	6
 서연            	4
 재욱            	3
 강교수           	2
 구소장           	2
 백피디           	2
 아버지           	2
 피디            	2
 기사            	1

> 이 나옵니다.

> * 반면 부정적인 대사를 하는 등장인물은

>승민            	25
 서연            	18
 은채            	12
 납뜩이           	8
 어머니           	6
 아버지           	3
 동구            	2
 구소장           	1
 기사            	1
 재욱            	1

> 순입니다. 

---------------------
#### 부산행
---------------------

> * 같은 코드로 데이터만 바꾸고 돌렸을 때 부산행에서 긍정적인 대사를 하는 등장인물

>지문            	50
 석우            	6
 석우모           	4
 상화            	2
 기장            	1
 기철            	1
 김 대리          	1
 줘…            	1
 용석            	1
 진희            	1
 팀장            	1
 
> * 부산행에서 부정적인 대사를 하는 등장인물

>지문            	115
 석우            	13
 노숙자           	9
 상화            	7
 수안            	4
 용석            	4
 영국            	3
 공익            	2
 기장            	2
 기철            	2
 김 대리          	2
 종길            	2
 나영            	1
 석우모           	1
 인길            	1
 진희            	1
 
--------------------------
### Positive / Negative / Plain 문장 비율 시각화
---------------------------------

>대본 파일 읽어오기

```
from pathlib import Path

#file = Path.joinpath('Resources', 'test.txt')
file = Path.cwd() / './data/건축학개론.txt'
#file.read_text()

    # Open the file in "read" mode ('r') 
with open(file, 'r') as text:
    textfile = text.read()
    print(textfile)
```

>형태소 태깅에 필요한 패키지 import

```
from konlpy.tag import Mecab
from konlpy.utils import pprint
```

>긍정 단어 사전 읽어오기

```
positive = Path.cwd() / './KnuSentiLex/pos_pol_word.txt'

with open(positive, 'r') as text:
    positive = text.read()
    print(positive)
```

```
pos = positive.split('\n')
print(pos)
```

```
print(pos[18]) #여기까지는 사전 설명이므로 제외
print(pos[19])

del pos[0:19]
print(pos)
```

```
#긍정 문장으로 나누기 위해 우선 문장별로 토큰을 나눈다. 줄바꿈으로 구분. 

text_sentence = textfile.split("\n")
print(text_sentence)
```

```
#문장단위로 나누었고 너무 짧은 문장에서는 정보를 알아내기 어렵다고 판단. 
#문장의 길이가 15이하인 문장 제거
sentence_filtered = [e for e in text_sentence if len(e) >15]
print("original sentence:" ,len(text_sentence))
print("sentence filtered:" ,len(sentence_filtered))
```

```
sentence_replace = []
for i in sentence_filtered:
    sentence = i.replace("\t"," ")
    sentence = sentence.replace(".", " ")
    sentence = sentence.replace("?", " ")
    sentence = sentence.replace("!", " ")
    sentence = sentence.replace("/" ," ")
    sentence = sentence.replace("-" ," ")
    sentence = sentence.replace(">" ," ")
    sentence = sentence.replace("<" ," ")
    sentence = sentence.replace("(" ," ")
    sentence = sentence.replace(")" ," ")
    sentence = sentence.replace("|" ," ")
    sentence_replace.append(sentence)
```

```
print(sentence_replace)
```

```
sentence_split = []
for sentence in sentence_replace:
    sentence = sentence.split(" ")
    sentence_split.append(sentence)

print(sentence_split[1:3])
#리스트 안에 리스트가 들어있는 다중리스트
print(sentence_split[966])
print(sentence_split[966] in pos)
print("positive word:",pos[4689])
#확인하면 분명히 환하게라는 단어가 존재하고 있음에도 불구하고 False가 뜨고 있음.
```

```
pos_word = [set(pos).intersection(i) for i in sentence_split]
print(pos_word)
print(len(pos_word))
#공백을 포함하여 구분하고 있는 것을 확인할 수 있음
```

```
print(pos_word[14])
print(sentence_filtered[14])
#문장의 순서는 제대로 일치하게 들어가 있음.

len(pos_word[14])
#길이가 2이상이라면 긍정단어를 포함하고 있음.
```

```
pos_sentence = []
for i in range(0,len(pos_word)):
    if len(pos_word[i]) >= 2:
        pos_sentence.append(sentence_filtered[i])
```

```
print(pos_sentence)
len(pos_sentence)
```

```
###평서문, 긍정문, 부정문의 비율을 시각화 하기 위해 부정단어 사전 읽어오고 분리

negative = Path.cwd() / './KnuSentiLex/neg_pol_word.txt'

with open(negative, 'r') as text:
    negative = text.read()
    print(negative)
```

```
neg = negative.split('\n')
print(neg)

print(neg[18]) #여기까지는 사전 설명이므로 제외
print(neg[19])

del neg[0:19]
print(neg)
```

```
neg_word = [set(neg).intersection(i) for i in sentence_split]

neg_sentence = []
for i in range(0,len(neg_word)):
    if len(neg_word[i]) >= 2:
        neg_sentence.append(sentence_filtered[i])
        
print(neg_sentence)
len(neg_sentence)
```

```
#sentence_filtered 에서 pos_sentence와 neg_sentence와 동일한 값을 제거하면 평서문이 될 것임.
plain = []
for i in range(0,len(sentence_filtered)):
    if sentence_filtered[i] not in pos_sentence:
        if sentence_filtered[i] not in neg_sentence:
            plain.append(sentence_filtered[i])
```

```
import matplotlib.pyplot as plt
```

```
plt.rc('font', family='Malgun Gothic')
size = [len(plain), len(pos_sentence), len(neg_sentence)]
label = ['Plain', 'Positive', 'Negative']

plt.axis('equal')
plt.pie(size, labels=label)
plt.show()
```

* 위 코드를 전부 실행하면 건축학개론의 긍부정 문장 비율이 나옵니다.


![건축학개론 대사 비율](https://pbs.twimg.com/media/EpRg_8nUcAEdIIr?format=png&name=360x360 "건축학개론 대사 비율")

* 위와 같은 방법으로 뽑은 부산행 긍부정 문장 비율입니다.

![부산행 대사 비율](https://pbs.twimg.com/media/EpRhKqPU0AEZulO?format=png&name=360x360 "부산행 대사 비율")

> 부산행의 분위기가 건축학개론보다 어둡다는 것을 알 수 있습니다.

---------------------------
### 인물 관계성 분석
--------------------------
#### 부산행

```
#한글 태깅을 위한 패키지 설치
import os
import numpy as np
import pandas as pd
from konlpy.tag import Okt

```

```
#네이버 리뷰 읽어오기
senti = pd.read_csv('material/nsmc/ratings.txt', sep='\t')
```

```
train_df = pd.read_csv('material/nsmc/ratings_train.txt', sep='\t')
test_df = pd.read_csv('material/nsmc/ratings_test.txt', sep='\t')
```

```
train_df.shape, test_df.shape
```

```
tagger = Okt()
```

```
# 형태소  태깅에 장시간이 소요됨으로 한번 시행 후 저장하여 활용
#neg=pos여야 하나 첫 실행 시 잘못된 변수에 저장을 했고, 변경이 불가능함.
train_df['neg'] = train_df['document'].map(lambda x : tagger.pos(str(x), norm=True, stem=True))
test_df['neg'] = test_df['document'].map(lambda x : tagger.pos(str(x), norm=True, stem=True))
train_df.to_csv('material/nsmc/ratings.txt', index=False)
test_df.to_csv('material/nsmc/ratings .txt', index=False)
```

```
train_df = pd.read_csv('material/nsmc/train.csv')
test_df = pd.read_csv('material/nsmc/test.csv')
```

```
#'neg' 열을 'pos'열로 바꾸기 위한 작업
train_df['pos']=train_df['neg']
train_df.drop(['neg'], axis='columns', inplace=True)
```

```
train_df.head()
```

```
#'neg' 열을 'pos'열로 바꾸기 위한 작업
test_df['pos']=test_df['neg']
test_df.drop(['neg'], axis='columns', inplace=True)
```

```
test_df.head()
```

```
from sklearn.feature_extraction.text import TfidfVectorizer # 벡터화
```

```
 vectorizer = TfidfVectorizer(max_features=5000) # colab RAM이 부족하여 5천개로 타협
vectorizer.fit(train_df['pos'])
train_X = vectorizer.transform(train_df['pos'])
test_X = vectorizer.transform(test_df['pos'])
```

```
len(vectorizer.get_feature_names())
```

```
 train_y = train_df['label']
test_y = test_df['label']
```

```
#모델링에 필요한 module import 
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras import optimizers
from tensorflow.keras import losses
from tensorflow.keras import metrics
```

```
model = models.Sequential()
model.add(layers.Dense(64, activation='relu', input_shape=(5000,)))
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))

model.compile(optimizer=optimizers.RMSprop(lr=0.001),
             loss=losses.binary_crossentropy,
             metrics=[metrics.binary_accuracy])

model.fit(train_X, train_y, epochs=10, batch_size=512)
results = model.evaluate(test_X, test_y) # 83%의 정확도 확보
```

> 모델 학습 완료

```
##대본 감성분석
#대본 불러오기
script = pd.read_csv('대본/부산행-지문분리.csv')
```
* 대본 감성분석을 위해 부산행.txt 파일을 csv로 변환했습니다.

```
script['pos'] = script['content'].map(lambda x : tagger.pos(str(x), norm=True, stem=True))
script.to_csv('material/nsmc/script.csv', index=False)
```

```
script.head()
```

* act와 content 테이블로 나뉘며 act에는 지문을 포함한 등장인물의 이름이, content에는 대사와 지문이 들어갑니다.

```
script = pd.read_csv('material/nsmc/script.csv')
```

```
tagger = Okt()
```

```
script_X = vectorizer.transform(script['pos'])
```

```
script_y = model.predict(script_X)
```

```
script['label'] = script_y
```

```
script.sort_values(by=['label'])
```

```
##대사 연관분석
main_actor = ['석우','상화','수안','용석','기장','성경']
```

* 부산행에서 대사의 지분이 많았던 상위 5명으로 main_actor 리스트를 생성합니다.

```
script_by_actor = pd.DataFrame(index = main_actor)
```

```
for actor in main_actor: # 인물별로 대사들의 품사 테깅 데이터를 통합
    script_by_actor.loc[actor, 'scripts'] = script[script['act'] == actor]['pos'].str.cat()
```

```
from sklearn.feature_extraction.text import TfidfVectorizer
```

```
corvec = TfidfVectorizer()
script_vec = corvec.fit_transform(script_by_actor['scripts'])
```

```
import scipy # 벡터거리 계산 함수 https://steemit.com/kr/@anpigon/5
def dist_norm(v1, v2):
    v1_normalized = v1/scipy.sparse.linalg.norm(v1)
    v2_normalized = v2/scipy.sparse.linalg.norm(v2)
    delta = v1_normalized - v2_normalized
    return scipy.sparse.linalg.norm(delta)
```

```
cor_mat = np.zeros([6,6]) # 계산된 값을 상관 행렬로 정리
for i in range(6):
    for j in range(6):
        cor_mat[i,j] = dist_norm(script_vec[i], script_vec[j])
```

```
! pip install seaborn
```
* 유사도를 시각화하기 위해 seaborn 패키지를 다운받습니다.

```
import seaborn as sns
sns.heatmap(cor_mat, annot=True) #석우','상화','수안','용석','기장','성경'
```

![부산행 대사 유사도](https://pbs.twimg.com/media/EpRgMqBUUAEBdKf?format=png&name=360x360 "부산행 대사 유사도")

> 상화와 석우의 대사가 유사함을 알 수 있습니다.

```
script[(script['act']=="상화") | (script['act']=="석우")].tail(50)
#대사가 유사함을 확인할 수 있음.
```
![부산행](https://pbs.twimg.com/media/EpRc2tPUYAIkVsZ?format=png&name=medium "부산행")
![부산행-2](https://pbs.twimg.com/media/EpRdR0eVgAMK8Wi?format=png&name=medium "부산행-2")

> 상화와 석우가 각각 한 아이의 부모로 유대감을 형성하고 있다는 것을 알 수 있습니다.

------------------
#### 건축학개론

> 위 코드 데이터만 변경

```
import seaborn as sns
sns.heatmap(cor_mat, annot=True) #'승민','서연','납뜩이','은채','어머니','재욱'
```
![건축학개론 대사 유사도](https://pbs.twimg.com/media/EpRgUPvU8AAiKAj?format=png&name=360x360 "건축학개론 대사 유사도")

> 승민과 서연의 대사가 유사함을 알 수 있습니다.

```
script[(script['act']=="승민") | (script['act']=="서연")].tail(50)
```
![건축학](https://pbs.twimg.com/media/EpRerwRVQAA_Wqr?format=png&name=medium "건축학")
![건축학-2](https://pbs.twimg.com/media/EpReyLTUcAAG96c?format=png&name=medium "건축학-2")

> 네이버 리뷰 감성 분석 모델링, 대사 유사도 분석 코드 참조: <https://github.com/jaygo-kr/brunch/blob/master/03_03_sentiment_and_association_analysis.ipynb>